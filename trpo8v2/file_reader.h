#ifndef FILE_READER_H
#define FILE_READER_H

#include "member_maraphone.h"

void read(const char* file_name, member_maraphone* array[], int& size);

#endif
