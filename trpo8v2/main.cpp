#include <iostream>
#include <iomanip>
#include "member_maraphone.h"
#include "file_reader.h"
#include "constans.h"
using namespace std;

int main()
{
    setlocale(LC_ALL, "ru");
    cout << "Laboratory work #8. GIT\n";
    cout << "Variant #11. Maraphon member\n";
    cout << "Author: Pozdeev Nikita\n";
    member_maraphone* member_maraphone[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("�����.txt", member_maraphone, size);
        for (int i = 0; i < size; i++)
        {
            cout << "***** ��������� �������� *****\n\n";
            //�������
            cout << member_maraphone[i]->member.last_name << '\n';
            //���
            cout << member_maraphone[i]->member.first_name << '\n';
            //��������
            cout << member_maraphone[i]->member.middle_name << '\n';
            // ����� ����
            cout << setw(4) << setfill('0') << member_maraphone[i]->start.hour << '-';
            // ����� �����
            cout << setw(2) << setfill('0') << member_maraphone[i]->start.minutes << '-';
            // ����� ������
            cout << setw(2) << setfill('0') << member_maraphone[i]->start.second;
            cout << '\n';
            /********** ����� ���� �������� **********/
            // ����� ����
            cout << "���� ��������...: ";
            cout << setw(4) << setfill('0') << member_maraphone[i]->finish.hour << '-';
            // ����� �����
            cout << setw(2) << setfill('0') << member_maraphone[i]->finish.minutes << '-';
            // ����� ������
            cout << setw(2) << setfill('0') << member_maraphone[i]->finish.second;
            cout << '\n';
            cout << member_maraphone[i]->club << '\n';
            cout << '\n';
            cout << '\n';
        }
        for (int i = 0; i < size; i++)
        {
            delete member_maraphone[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}
